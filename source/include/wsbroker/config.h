/*
 * wsbroker/config.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_CONFIG_H_
#define WSBROKER_CONFIG_H_


#include <vector>

#include <x/common.h>
#include <x/io/json.h>


namespace wsbroker
{
  using namespace x;
  using namespace x::io;


  struct ConfigRoute
  {
    Int sourceId;
    Int targetId;

    Boolean buffered;
    Boolean ordered;
  };

  struct ConfigHandler
  {
    Int id;
    String module;

    Json::Value config;
  };

  struct ConfigGlobal
  {
    std::vector<ConfigHandler> handlers;
    std::vector<ConfigRoute> routes;

    Json::Value defaults;
  };


  inline Json::Value const& config_get(Json::Value const& config, String const& key)
  {
    return config.at(key);
  }

  inline Json::Value const& config_get(Json::Value const& config, String const& key, Json::Value defaultValue)
  {
    return config.at(key, defaultValue);
  }

  inline Json::Value const& config_get(ConfigGlobal const& config, String const& key)
  {
    return config_get(config.defaults, key);
  }

  inline Json::Value const& config_get(ConfigHandler const& config, String const& key)
  {
    return config_get(config.config, key);
  }

  inline Json::Value const& config_get(ConfigGlobal const& configGlobal, ConfigHandler const& configHandler, String const& key)
  {
    if(configHandler.config.has(key))
      return configHandler.config.at(key);

    return configGlobal.defaults.at(key);
  }


  template<typename T>
  inline T config_get(Json::Value const& config, String const& key, T const& defaultValue)
  {
    return config.at<T>(key, defaultValue);
  }

  template<typename T>
  inline String config_get(Json::Value const& config, String const& key, Char const* const& defaultValue)
  {
    return config.at<String>(key, defaultValue);
  }

  template<typename T>
  inline T config_get(Json::Value const& configGlobal, Json::Value const& configHandler, String const& key, T const& defaultValue)
  {
    if(configHandler.has<T>(key))
      return configHandler.at<T>(key, defaultValue);

    return configGlobal.at<T>(key, defaultValue);
  }

  template<typename T>
  inline String config_get(Json::Value const& configGlobal, Json::Value const& configHandler, String const& key, Char const* const& defaultValue)
  {
    if(configHandler.has<String>(key))
      return configHandler.at<String>(key, defaultValue);

    return configGlobal.at<String>(key, defaultValue);
  }

  template<typename T>
  inline T config_get(ConfigGlobal const& config, String const& key, T const& defaultValue)
  {
    return config_get<T>(config.defaults, key, defaultValue);
  }

  template<typename T>
  inline T config_get(ConfigHandler const& config, String const& key, T const& defaultValue)
  {
    return config_get<T>(config.config, key, defaultValue);
  }

  template<typename T>
  inline T config_get(ConfigGlobal const& configGlobal, ConfigHandler const& configHandler, String const& key, T const& defaultValue)
  {
    return config_get<T>(configGlobal.defaults, configHandler.config, key, defaultValue);
  }

  template<typename T>
  inline String config_get(ConfigGlobal const& configGlobal, ConfigHandler const& configHandler, String const& key, Char const* const& defaultValue)
  {
    return config_get<String>(configGlobal.defaults, configHandler.config, key, defaultValue);
  }
}


#define WSBROKER_CONFIG(config, key)                                            \
  wsbroker::config_get<decltype(WSBROKER_CONFIG_DEFAULT_##key)>(                \
    config,                                                                     \
    WSBROKER_CONFIG_KEY_##key,                                                  \
    WSBROKER_CONFIG_DEFAULT_##key)

#define WSBROKER_CONFIGV(config, key)                                           \
  wsbroker::config_get(                                                         \
    config,                                                                     \
    WSBROKER_CONFIG_KEY_##key)

#define WSBROKER_CONFIGS(config, configHandler, key)                            \
  wsbroker::config_get<decltype(WSBROKER_CONFIG_DEFAULT_##key)>(                \
    config,                                                                     \
    configHandler,                                                              \
    WSBROKER_CONFIG_KEY_##key,                                                  \
    WSBROKER_CONFIG_DEFAULT_##key)

#define WSBROKER_CONFIGS_OR(config, configHandler, key, defaultValue)           \
  wsbroker::config_get<decltype(defaultValue)>(                                 \
    config,                                                                     \
    configHandler,                                                              \
    WSBROKER_CONFIG_KEY_##key,                                                  \
    defaultValue)


#endif
