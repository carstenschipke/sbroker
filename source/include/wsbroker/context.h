/*
 * wsbroker/context.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_CONTEXT_H_
#define WSBROKER_CONTEXT_H_


#include <map>

#include <x/common.h>
#include <x/common/modules.h>

#include <wsbroker/config.h>
#include <wsbroker/module.h>
#include <wsbroker/stream.h>


namespace wsbroker
{
  using namespace x;
  using namespace x::common;


  struct Context
  {
    Modules<BrokerModule> modules;
    ConfigGlobal config;

    Stream* router;
  };
}


#endif
