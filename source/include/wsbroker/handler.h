/*
 * wsbroker/handler.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_HANDLER_H_
#define WSBROKER_HANDLER_H_


#include <wsbroker/message.h>
#include <wsbroker/stream.h>


namespace wsbroker
{
  class Handler: public Stream
  {
    public:
      virtual void start() = 0;
      virtual void stop() = 0;
  };
}


#endif
