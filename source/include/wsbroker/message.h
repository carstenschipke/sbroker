/*
 * wsbroker/message.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_MESSAGE_H_
#define WSBROKER_MESSAGE_H_


#include <x/common.h>


#ifndef HANDLER_LIMIT
# define HANDLER_LIMIT 100
#endif
#ifndef ASCII_OFFSET_ZERO
# define ASCII_OFFSET_ZERO 48
#endif

#ifndef SIZE_MESSAGE_HANDLER_ID
# define SIZE_MESSAGE_HANDLER_ID 2
#endif


namespace wsbroker
{
  using namespace x;


  struct Message
  {
    Int target;
    Int source;

    String body;

    String sid;
  };


  inline Int message_extract_target(String const& body)
  {
#   if 1<SIZE_MESSAGE_HANDLER_ID
      auto id0=std::max(0, ((Int)body[0])-ASCII_OFFSET_ZERO);
      auto id1=std::max(0, ((Int)body[1])-ASCII_OFFSET_ZERO);

      return std::min(HANDLER_LIMIT, 10*id0+id1);
#   else
#     if 0<SIZE_MESSAGE_HANDLER_ID
        return std::max(0, ((Int)body[0])-ASCII_OFFSET_ZERO);
#     else
        return 0;
#     endif
#   endif
  }


  inline Message& operator<<(Message& message, String const& body)
  {
    message.target=message_extract_target(body);
    message.body=body.substr(SIZE_MESSAGE_HANDLER_ID);

    return message;
  }

  inline std::ostream& operator<<(std::ostream& stream, Message const& message)
  {
    stream << "Message{"
      <<   "source: " << message.source
      <<   ", target: " << message.target
      <<   ", body: " << message.body
      <<   ", sid: " << message.sid
      << "}";

    return stream;
  }
}


#endif
