/*
 * wsbroker/module.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_MODULE_H_
#define WSBROKER_MODULE_H_


#include <x/common.h>
#include <x/common/module.h>
#include <x/io/json.h>

#include <wsbroker/config.h>
#include <wsbroker/handler.h>
#include <wsbroker/stream.h>


namespace wsbroker
{
  using namespace x;
  using namespace x::common;
  using namespace x::io;


  struct Context;


  class BrokerModule: public Module
  {
    public:
      virtual ~BrokerModule()
      {

      }


      virtual std::unique_ptr<Handler> handler(Json::Value const& configDefaults, Json::Value const& config, Stream* const sink) = 0;
  };
}


#endif
