/*
 * wsbroker/stream.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_STREAM_H_
#define WSBROKER_STREAM_H_


#include <x/common.h>

#include <wsbroker/message.h>


namespace wsbroker
{
  using namespace std;

  using namespace x;


  class Stream
  {
    public:
      virtual void operator<<(Message& message) = 0;
  };


  class NullStream: public Stream
  {
    public:
      virtual void operator<<(Message& message) override
      {
#       if defined(X_DEBUG) && 3<X_DEBUG
          mout("[ ] DROP " << message);
#       endif
      }
  };


  class ForwardingStream: public Stream
  {
    public:
      ForwardingStream(Stream* stream):
        m_stream(stream)
      {

      }

      virtual ~ForwardingStream()
      {

      }


      virtual void operator<<(Message& message) override
      {
        *m_stream << message;
      }


    protected:
      Stream* m_stream;
  };
}


#endif
