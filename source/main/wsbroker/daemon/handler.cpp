/*
 * daemon/handler.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <unistd.h>

#include "handler.h"


namespace wsbroker
{
  namespace daemon
  {
    Handler::Handler(Context* const context, ConfigHandler const& config):
      Task("handler-"+to_string(config.id)),
      id(config.id),
      m_sink(id, context->router),
      m_impl(context->modules.get(config.module)->handler(context->config.defaults, config.config, &m_sink))
    {

    }


    void Handler::stop()
    {
      m_impl->stop();

      interrupt();
    }



    void Handler::operator<<(Message& message)
    {
      mout("[" << id << "] RECV " << message);

      *m_impl << message;
    }


    void Handler::run()
    {
//#     if defined(X_DEBUG) && 2<X_DEBUG
//        auto future=async(launch::async, [this]{
//          while(!interrupted())
//          {
//            dbgsi("[" << id << "] QUEU " << m_queue.size());
//
//            usleep(chrono::microseconds{chrono::milliseconds{500}}.count());
//          }
//        });
//
//        future.wait_for(chrono::nanoseconds{1});
//#     endif

      m_impl->start();

      wait(State::INTERRUPTED);
    }
  }
}
