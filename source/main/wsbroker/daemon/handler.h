/*
 * daemon/handler.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_DAEMON_HANDLER_H_
#define WSBROKER_DAEMON_HANDLER_H_


#include <queue>

#include <config.h>

#include <x/common.h>
#include <x/common/modules.h>

#include <wsbroker/context.h>
#include <wsbroker/module.h>
#include <wsbroker/stream.h>

#include <x/concurrent/task.h>


namespace wsbroker
{
  namespace daemon
  {
    using namespace std;

    using namespace x;
    using namespace x::common;
    using namespace x::concurrent;


    class SourceStream: public Stream
    {
      public:
        SourceStream(Int id, Stream* stream):
          m_id(id), m_stream(stream)
        {

        }


        virtual void operator<<(Message& message) override
        {
          message.source=m_id;

          mout("[" << m_id << "] RECV " << message);

          *m_stream << message;
        }


      private:
        Int m_id;
        Stream* m_stream;
    };


    class Handler: public Task, public Stream
    {
      public:
        Int const id;


        using Task::Task;


        Handler(Context* const context, ConfigHandler const& config);


        virtual ~Handler()
        {

        }


        void stop();


        virtual void operator<<(Message& message) override;


      protected:
        virtual void run() override;


      private:
        SourceStream m_sink;
        std::unique_ptr<::wsbroker::Handler> m_impl;
    };
  }
}


#endif
