/*
 * daemon/main.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <signal.h>
#include <unistd.h>

#include <boost/program_options.hpp>

#include <config.h>

#include <x/common.h>
#include <x/concurrent/process.h>
#include <x/io/file.h>

#include "supervisor.h"


using namespace std;

using namespace x;
using namespace x::common;
using namespace x::concurrent;
using namespace x::io;

namespace options=boost::program_options;


class Daemon: public Process<Int>
{
  public:
    using Process::Process;


    Daemon(String const& pathConfigFile):
      Process(WSBROKER_NAME_DAEMON), m_supervisor(pathConfigFile)
    {
      uid(WSBROKER_UID);
      gid(WSBROKER_GID);
    }


  protected:
    virtual Int call() override
    {
      static auto supervisor=&m_supervisor;

      auto signalHandler=[](Int signal) {
        if(SIGUSR1==signal)
          supervisor->signal(wsbroker::daemon::Signal::RELOAD);
        else
          supervisor->signal(wsbroker::daemon::Signal::STOP);
      };

      signal(SIGINT, signalHandler);
      signal(SIGTERM, signalHandler);
      signal(SIGQUIT, signalHandler);
      signal(SIGUSR1, signalHandler);

      return supervisor->run();
    }


  private:
    wsbroker::daemon::Supervisor m_supervisor;
};


void daemon_detach()
{
  daemon(0, 0);
}

String daemon_pid_path()
{
  return String{WSBROKER_PATH_PID}+"/"+String{WSBROKER_NAME_DAEMON}+".pid";
}

void daemon_pid_clear()
{
  unlink(daemon_pid_path().c_str());
}

Int daemon_pid_read()
{
  File pidFile{daemon_pid_path()};

  if(!pidFile.exists())
    return -1;

  return stoi(pidFile.readText());
}

Int daemon_pid_write()
{
  std::ofstream pidFile{daemon_pid_path(), std::ios::trunc};

  if(pidFile.good())
  {
    auto pid=getpid();

    pidFile << std::to_string(pid);
    pidFile.close();

    return pid;
  }

  return -1;
}

Boolean daemon_signal(wsbroker::daemon::Signal signal)
{
  auto pid=daemon_pid_read();

  if(0>pid)
    return false;

  switch(signal)
  {
    case wsbroker::daemon::Signal::STOP:
      kill(pid, SIGQUIT);
      return true;
    case wsbroker::daemon::Signal::RELOAD:
      sigqueue(pid, SIGUSR1, {signal});
      return true;
    default:
      return false;
  }
}

Boolean daemon_signal(String const& signal)
{
  return daemon_signal(wsbroker::daemon::to_signal(signal));
}


Int main(Int argc, Char* const argv[])
{
  String version{X_VERSION_STRING};

  auto configPath=String{WSBROKER_PATH_CONFIG};
  auto configFilePath=String{configPath+"/"+WSBROKER_FILE_CONFIG_DAEMON};

  auto descriptionConfigFilePath="set configuration file (default: "+configFilePath+")";
  auto descriptionSignals="send signal ["+wsbroker::daemon::signal_names()+"]";

  options::variables_map args;
  options::options_description description{"Options"};

  description.add_options()
    ("help,h", "this help")
    ("version,v", "show version and exit")
    ("daemon,d", "launch as daemon")
    ("signal,s", options::value<String>(), descriptionSignals.c_str())
    ("config,c", options::value<String>(), descriptionConfigFilePath.c_str());

  try
  {
    options::store(options::parse_command_line(argc, argv, description), args);
    options::notify(args);
  }
  catch(exception const& e)
  {
    mout(version << endl << endl << description);

    return EXIT_FAILURE;
  }

  if(0<args.count("help"))
  {
    mout(version << endl << endl << description);

    return EXIT_SUCCESS;
  }

  if(0<args.count("version"))
  {
    mout(version);

    return EXIT_SUCCESS;
  }

  if(0<args.count("signal"))
  {
    auto signal=args.at("signal").as<String>();

    if(!daemon_signal(signal))
      return EXIT_FAILURE;

    return EXIT_SUCCESS;
  }


  auto const daemonize=0<args.count("daemon");

  if(daemonize)
    daemon_detach();

  if(0>daemon_pid_write())
  {
    mout("Unable to write PID file [" << daemon_pid_path() << "].");

    return EXIT_FAILURE;
  }


  if(0<args.count("config"))
    configFilePath=args.at("config").as<String>();


  Daemon daemon{configFilePath};

  static auto self=&daemon;

  auto signalHandler=[](Int signal) {
    sigqueue(self->pid(), signal, {});
  };

  signal(SIGINT, signalHandler);
  signal(SIGTERM, signalHandler);
  signal(SIGQUIT, signalHandler);
  signal(SIGUSR1, signalHandler);


  if(daemon.start())
    daemon.wait();

  daemon_pid_clear();


  if(daemon.completed())
    return EXIT_SUCCESS;

  return EXIT_FAILURE;
}
