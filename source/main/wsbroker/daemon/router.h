/*
 * daemon/router.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_DAEMON_ROUTER_H_
#define WSBROKER_DAEMON_ROUTER_H_


#include <atomic>
#include <chrono>
#include <cstring>
#include <queue>
#include <vector>

#include <config.h>

#include <x/common.h>

#include <wsbroker/context.h>
#include <wsbroker/message.h>
#include <wsbroker/stream.h>

#include "handler.h"


namespace wsbroker
{
  namespace daemon
  {
    using namespace std;

    using namespace x;
    using namespace x::common;
    using namespace x::concurrent;


    class Router: public Stream
    {
      public:
        Router()
        {
          Size const limit=HANDLER_LIMIT+1;

          m_routes.resize(limit);

          for(Size x=0; x<limit; x++)
          {
            m_routes[x].resize(limit);

            for(Size y=0; y<limit; y++)
              m_routes[x][y]=&m_streamNull;
          }
        }


        void start()
        {

        }

        void stop()
        {

        }


        void add(ConfigRoute& config, Handler* source, Handler* target)
        {
          mout("[X] Add route for handlers [source: " << source->id << ", target: " << target->id << "].");

          m_routes[source->id][target->id]=target;
        }

        void add(ConfigRoute& config, Handler* target)
        {
          mout("[X] Add route for handlers [source: 0, target: " << target->id << "].");

          m_routes[0][target->id]=target;
        }

        void remove(Int handlerId)
        {
          mout("[X] Remove routes related to handler [handler: " << handlerId << "].");

          for(Size x=0; x<m_routes.size(); x++)
          {
            m_routes[x][handlerId]=&m_streamNull;
            m_routes[handlerId][x]=&m_streamNull;
          }
        }


        virtual void operator<<(Message& message) override
        {
          mout("[X] RECV " << message);

          *(m_routes[message.source][message.target]) << message;
        }


      private:
        vector<vector<Stream*>> m_routes;
        NullStream m_streamNull;
    };
  }
}


#endif
