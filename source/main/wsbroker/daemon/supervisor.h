/*
 * daemon/supervisor.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef WSBROKER_DAEMON_SUPERVISOR_H_
#define WSBROKER_DAEMON_SUPERVISOR_H_


#include <atomic>
#include <vector>

#include <config.h>

#include <x/common.h>
#include <x/common/modules.h>
#include <x/io/resources.h>
#include <x/io/resource/json.h>
#include <x/io/file.h>
#include <x/misc/string.h>

#include <wsbroker/config.h>
#include <wsbroker/context.h>

#include "handler.h"
#include "router.h"


namespace wsbroker
{
  namespace daemon
  {
    using namespace std;

    using namespace x;
    using namespace x::common;
    using namespace x::io;


    enum Signal: Int
    {
      VOID,
      STOP,
      RELOAD
    };


    Int const SIGNALS_COUNT=3;

    Signal const SIGNALS[SIGNALS_COUNT]={
      Signal::VOID,
      Signal::STOP,
      Signal::RELOAD
    };

    String const SIGNAL_NAMES[SIGNALS_COUNT]={
      "VOID",
      "STOP",
      "RELOAD"
    };


    inline String signal_names(String const& delimiter="|")
    {
      return misc::string::join(delimiter, {SIGNAL_NAMES[1], SIGNAL_NAMES[2]});
    }

    inline Signal to_signal(Int signal)
    {
      return SIGNALS[signal];
    }

    inline Signal to_signal(String signal)
    {
      for(auto i=0; i<SIGNALS_COUNT; i++)
      {
        if(0==strcasecmp(SIGNAL_NAMES[i].c_str(), signal.c_str()))
          return to_signal(i);
      }

      return Signal::VOID;
    }


    inline void parse_id_enum(vector<Int>& ids, String const& config, Char const& delimiter)
    {
      vector<String> idsEnum;

      misc::string::split(idsEnum, config, delimiter);

      for(auto& id : idsEnum)
        ids.push_back(stoi(id));
    }

    inline void parse_id_range(vector<Int>& ids, String const& config, Char const& delimiter)
    {
      vector<String> idsMinMax;

      misc::string::split(idsMinMax, config, delimiter);

      if(2==idsMinMax.size())
      {
        auto limit=stoi(idsMinMax.back());

        for(auto i=stoi(idsMinMax.front()); i<limit; i++)
          ids.push_back(i);
      }
      else if(1==idsMinMax.size())
      {
        ids.push_back(stoi(idsMinMax.front()));
      }
    }

    inline void parse_ids(vector<Int>& ids, String const& config)
    {
      if(String::npos==config.find('-'))
        parse_id_enum(ids, config, ',');
      else
        parse_id_range(ids, config, '-');
    }


    static void handlers_add(vector<ConfigHandler>& handlers, Json::Value const& config)
    {
      vector<Int> ids;

      if(config.has<String>(WSBROKER_CONFIG_KEY_HANDLER_ID))
        parse_ids(ids, config.at<String>(WSBROKER_CONFIG_KEY_HANDLER_ID, ""));
      else
        ids.push_back(config.at<Int>(WSBROKER_CONFIG_KEY_HANDLER_ID, WSBROKER_CONFIG_DEFAULT_HANDLER_ID));

      for(auto& id : ids)
      {
        handlers.push_back({
          id,
          WSBROKER_CONFIG(config, HANDLER_MODULE),
          WSBROKER_CONFIGV(config, HANDLER_CONFIG)
        });
      }
    }

    static void routes_add(vector<ConfigRoute>& routes, Json::Value const& config)
    {
      vector<Int> idsSource;
      vector<Int> idsTarget;

      if(config.has<String>(WSBROKER_CONFIG_KEY_ROUTE_SOURCE))
        parse_ids(idsSource, config.at<String>(WSBROKER_CONFIG_KEY_ROUTE_SOURCE, ""));
      else
        idsSource.push_back(WSBROKER_CONFIG(config, ROUTE_SOURCE));

      if(config.has<String>(WSBROKER_CONFIG_KEY_ROUTE_TARGET))
        parse_ids(idsTarget, config.at<String>(WSBROKER_CONFIG_KEY_ROUTE_TARGET, ""));
      else
        idsTarget.push_back(WSBROKER_CONFIG(config, ROUTE_TARGET));

      for(auto& source : idsSource)
      {
        for(auto& target : idsTarget)
        {
          routes.push_back({
            source,
            target,
            WSBROKER_CONFIG(config, ROUTE_BUFFERED),
            WSBROKER_CONFIG(config, ROUTE_ORDERED)
          });
        }
      }
    }


    class Supervisor
    {
      public:
        Supervisor(String const& pathConfigFile):
          m_pathConfigFile(pathConfigFile),
          m_context({{WSBROKER_MODULE_PREFIX}})
        {
          m_context.router=&m_router;
        }


        Int run()
        {
          auto status=reloadImpl();

          if(EXIT_SUCCESS!=status)
            return status;


          m_router.start();


          while(true)
          {
            if(m_listener.empty())
              return EXIT_FAILURE;

            for(auto& handler : m_listener)
              handler.second->start();


            unique_lock<decltype(m_signalMonitor)> lock{m_signalMonitor};

            m_signalUpdated.wait(lock);

            if(Signal::STOP==m_signal)
              break;
            else if(Signal::RELOAD==m_signal)
              reloadImpl();
          }


          for(auto& handler : m_listener)
            handler.second->stop();

          for(auto& handler : m_listener)
            handler.second->wait(chrono::seconds{WSBROKER_THREAD_TIMEOUT_STOP});


          m_router.stop();


          return EXIT_SUCCESS;
        }


        void signal(Signal signal)
        {
          m_signal=signal;
          m_signalUpdated.notify_all();
        }


      private:
        Resources m_resources;
        String m_pathConfigFile;
        Context m_context;

        atomic<Signal> m_signal;
        condition_variable m_signalUpdated;
        mutex m_signalMonitor;

        Router m_router;

        map<Int, Handler*> m_listener;
        map<Int, unique_ptr<Handler>> m_handler;


        Int reloadImpl()
        {
          mout("Resolving configuration [" << m_pathConfigFile << "].");

          auto handler=m_resources.get<resource::Json>("file://"+m_pathConfigFile);
          auto& config=handler.resolve();

          if(handler.impl().hasError())
          {
            mout("Unable to parse configuration [error: " << handler.impl().error() << ", source: " << handler.resource().source().toString() << "].");

            return EXIT_FAILURE;
          }

          if(!WSBROKER_CONFIGV(config, HANDLER).isArray()
            || 1>WSBROKER_CONFIGV(config, HANDLER).size())
          {
            mout("No handler defined in configuration [file: " << m_pathConfigFile << "].");

            return EXIT_FAILURE;
          }

          if(!WSBROKER_CONFIGV(config, ROUTE).isArray()
            || 1>WSBROKER_CONFIGV(config, ROUTE).size())
          {
            mout("No routes defined in configuration [file: " << m_pathConfigFile << "].");

            return EXIT_FAILURE;
          }

          m_context.config.handlers.clear();
          for(auto& handler : WSBROKER_CONFIGV(config, HANDLER))
            handlers_add(m_context.config.handlers, handler);

          m_context.config.routes.clear();
          for(auto& route : WSBROKER_CONFIGV(config, ROUTE))
            routes_add(m_context.config.routes, route);

          m_context.config.defaults=WSBROKER_CONFIGV(config, DEFAULT);


          for(auto& configHandler : m_context.config.handlers)
          {
            if(m_context.modules.load(configHandler.module))
            {
              if(m_handler.end()==m_handler.find(configHandler.id))
                m_handler[configHandler.id]=make_unique<Handler>(&m_context, configHandler);
            }
            else
            {
              mout("Unable to initialize handler module ["
                << "handler: " << configHandler.id
                << ", module: " << configHandler.module
                << "].");
            }
          }


          // TODO Clean unreferenced instances [handlers, listeners].


          for(auto& configRoute : m_context.config.routes)
          {
            if(0<configRoute.targetId)
            {
              if(m_handler.end()==m_handler.find(configRoute.targetId))
              {
                m_router.remove(configRoute.targetId);
              }
              else if(0<configRoute.sourceId)
              {
                if(m_handler.end()==m_handler.find(configRoute.sourceId))
                  m_router.remove(configRoute.sourceId);
                else
                  m_router.add(configRoute, m_handler[configRoute.sourceId].get(), m_handler[configRoute.targetId].get());
              }
              else
              {
                m_listener[configRoute.targetId]=m_handler[configRoute.targetId].get();

                m_router.add(configRoute, m_handler[configRoute.targetId].get());
              }
            }
          }


          return EXIT_SUCCESS;
        }
    };
  }
}


#endif
