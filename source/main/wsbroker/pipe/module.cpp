/*
 * pipe/module.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <iostream>
#include <fstream>

#include <config.h>

#include <x/common.h>

#include <wsbroker/context.h>


namespace wsbroker
{
  namespace pipe
  {
    using namespace std;


    struct Config
    {
      String sink;
    };


    class Sink
    {
      public:
        Sink():
          m_streamPtr(new ofstream("/dev/null", ios::out)),
          m_stream(*m_streamPtr)
        {

        }

        Sink(ostream& stream):
          m_streamPtr(&stream),
          m_stream(stream)
        {

        }


        void operator<<(Message& message)
        {
          m_stream << message;
        }


      private:
        ostream* m_streamPtr;
        ostream& m_stream;
    };


    class PipeHandler: public Handler
    {
      public:
        PipeHandler():
          m_sink()
        {

        }

        PipeHandler(ostream& stream):
          m_sink(stream)
        {

        }


        virtual void start() override
        {
          dbgi("Start handler [pipe].");
        }

        virtual void stop() override
        {
          dbgi("Stop handler [pipe].");
        }


        virtual void operator<<(Message& message) override
        {
          m_sink << message;
        }


      private:
        Sink m_sink;
    };


    class PipeModule: public BrokerModule
    {
      public:
        virtual unique_ptr<Handler> handler(Json::Value const& configDefaults, Json::Value const& config, Stream* const sink) override
        {
          Config configPipe{
            WSBROKER_CONFIGS(configDefaults, config, HANDLER_PIPE_SINK)
          };

          if(0==strcmp("stdout", configPipe.sink.c_str()))
            return make_unique<PipeHandler>(cout);

          return make_unique<PipeHandler>();
        }
    };
  }


  X_MODULE(pipe::PipeModule, "pipe", "1.0.0");
}
