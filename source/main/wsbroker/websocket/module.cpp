/*
 * websocket/module.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <unistd.h>

#include <cstring>
#include <map>

#include <config.h>

#include <x/common.h>
#include <x/misc/string.h>

#include <wsbroker/context.h>
#include <wsbroker/stream.h>

#include <websocketpp/config/asio.hpp>
#include <websocketpp/server.hpp>


#ifndef WEBSOCKET_HEADER_SESSION_ID
# define WEBSOCKET_HEADER_SESSION_ID "Sec-WebSocket-Key"
#endif


namespace wsbroker
{
  namespace websocket
  {
    using namespace std;

    using namespace x;
    using namespace x::misc;


    typedef boost::asio::io_service WsServer;

    typedef websocketpp::config::asio WsImpl;
    typedef websocketpp::config::asio_tls WssImpl;

    typedef websocketpp::server<WsImpl> WsEndpoint;
    typedef websocketpp::server<WssImpl> WssEndpoint;

    typedef websocketpp::lib::asio::ssl::context::password_purpose PasswordPurpose;
    typedef websocketpp::lib::asio::ssl::context WssContext;

    typedef websocketpp::connection_hdl WsConnectionHandler;
    typedef websocketpp::message_buffer::message<websocketpp::message_buffer::alloc::con_msg_manager> WsMessageBuffer;
    typedef websocketpp::lib::shared_ptr<WsMessageBuffer> WsMessage;


    struct WsConfig
    {
      Int port;
      String host;
    };


    struct WssConfig
    {
      Boolean enabled;

      Int port;
      String host;

      String dhPrime;
      String protocols;
      String ciphers;
      String certificateChain;
      String certificateKey;
      String certificateKeyPassphrase;
    };


    class WebsocketHandler: public Handler
    {
      private:
        void (WebsocketHandler::* SEND_MSG_WS)(Message&)=&WebsocketHandler::sendWs;
        void (WebsocketHandler::* SEND_MSG_WSS)(Message&)=&WebsocketHandler::sendWss;


        struct Channel
        {
          decltype(SEND_MSG_WS)* method;
          WsConnectionHandler connection;
        };


      public:
        WebsocketHandler(WsConfig const& configWs, Stream* const sink):
          WebsocketHandler(configWs, WssConfig{false}, sink)
        {

        }

        WebsocketHandler(WsConfig const& configWs, WssConfig const& configWss, Stream* const sink):
          m_configWs(configWs), m_configWss(configWss), m_sink(sink)
        {

        }


        virtual void start() override
        {
          dbgi("Start handler [websocket].");


          m_ws.set_reuse_addr(true);
          m_ws.init_asio(&m_server);

          auto closeHandlerWs=[this](WsConnectionHandler connectionHandler) {
            auto connection=m_ws.get_con_from_hdl(connectionHandler);
            auto sid=connection->get_request_header(WEBSOCKET_HEADER_SESSION_ID);

            connection->terminate({});

            m_channel.erase(sid);
          };

          m_ws.set_fail_handler(closeHandlerWs);
          m_ws.set_close_handler(closeHandlerWs);
          m_ws.set_interrupt_handler(closeHandlerWs);

          m_ws.set_message_handler([this](WsConnectionHandler connectionHandler, WsMessage message) {
            auto connection=m_ws.get_con_from_hdl(connectionHandler);
            auto sid=connection->get_request_header(WEBSOCKET_HEADER_SESSION_ID);

            Message msg;
            msg.sid=sid;
            msg << message->get_payload();

            *m_sink << msg;
          });

          m_ws.set_open_handler([this](WsConnectionHandler connectionHandler) {
            auto connection=m_ws.get_con_from_hdl(connectionHandler);
            auto sid=connection->get_request_header(WEBSOCKET_HEADER_SESSION_ID);

            m_channel.emplace(sid, Channel{&SEND_MSG_WS, connectionHandler});
          });

          m_ws.listen(m_configWs.port);
          m_ws.start_accept();


          if(m_configWss.enabled)
          {
            m_wss.set_reuse_addr(true);
            m_wss.init_asio(&m_server);

            m_wss.set_tls_init_handler([this](WsConnectionHandler connectionHandler) {
              return init(connectionHandler);
            });

            auto closeHandlerWss=[this](WsConnectionHandler connectionHandler) {
              auto connection=m_wss.get_con_from_hdl(connectionHandler);
              auto sid=connection->get_request_header(WEBSOCKET_HEADER_SESSION_ID);

              connection->terminate({});

              m_channel.erase(sid);
            };

            m_wss.set_fail_handler(closeHandlerWss);
            m_wss.set_close_handler(closeHandlerWss);
            m_wss.set_interrupt_handler(closeHandlerWss);

            m_wss.set_message_handler([this](WsConnectionHandler connectionHandler, WsMessage message) {
              auto connection=m_wss.get_con_from_hdl(connectionHandler);
              auto sid=connection->get_request_header(WEBSOCKET_HEADER_SESSION_ID);

              Message msg;
              msg.sid=sid;
              msg << message->get_payload();

              *m_sink << msg;
            });

            m_wss.set_open_handler([this](WsConnectionHandler connectionHandler) {
              auto connection=m_wss.get_con_from_hdl(connectionHandler);
              auto sid=connection->get_request_header(WEBSOCKET_HEADER_SESSION_ID);

              m_channel.emplace(sid, Channel{&SEND_MSG_WSS, connectionHandler});
            });

            m_wss.listen(m_configWss.port);
            m_wss.start_accept();
          }

          dbgi("Listening.");

          m_server.run();

          dbgi("Stopped.");
        }

        virtual void stop() override
        {
          dbgi("Stop handler [websocket].");

          m_server.stop();
        }


        virtual void operator<<(Message& message) override
        {
          (this->**(m_channel[message.sid].method))(message);
        }


      private:
        WsConfig m_configWs;
        WssConfig m_configWss;

        WsServer m_server;
        WsEndpoint m_ws;
        WssEndpoint m_wss;

        Stream* const m_sink;

        map<String, Channel> m_channel;


        void sendWs(Message& message)
        {
          m_ws.send(m_channel[message.sid].connection, message.body, websocketpp::frame::opcode::value::TEXT);
        }

        void sendWss(Message& message)
        {
          m_wss.send(m_channel[message.sid].connection, message.body, websocketpp::frame::opcode::value::TEXT);
        }


        // TODO Configure ssl/tls ciphers!?
        websocketpp::lib::shared_ptr<WssContext> init(WsConnectionHandler connectionHandler)
        {
          websocketpp::lib::shared_ptr<WssContext> context=
            websocketpp::lib::make_shared<WssContext>(WssContext::sslv23_server);


          // FIXME Issue with exported symbols when using initializer_lists / static initialisation.
          map<String, Long> sslProtocolsDisableBits;
          sslProtocolsDisableBits["TLSv1"]=WssContext::no_tlsv1;
          sslProtocolsDisableBits["TLSv1.1"]=WssContext::no_tlsv1_1;
          sslProtocolsDisableBits["TLSv1.2"]=WssContext::no_tlsv1_2;
          sslProtocolsDisableBits["SSLv2"]=WssContext::no_sslv2;
          sslProtocolsDisableBits["SSLv3"]=WssContext::no_sslv3;

          auto options=WssContext::default_workarounds
            |WssContext::no_sslv2
            |WssContext::no_sslv3
            |WssContext::no_tlsv1
            |WssContext::no_tlsv1_1
            |WssContext::no_tlsv1_2
            |WssContext::single_dh_use;

          vector<String> protocolsEnabled;
          vector<String> protocolsConfigured;

          misc::string::split(protocolsConfigured, m_configWss.protocols, ' ');

          for(auto& entry : sslProtocolsDisableBits)
          {
            for(auto& protocol : protocolsConfigured)
            {
              if(0==strcmp(entry.first.c_str(), protocol.c_str()))
                options&=~entry.second;
            }

            if(1>(options&entry.second))
              protocolsEnabled.push_back(entry.first);
          }

          context->set_options(options);

          mout("websocket/protocols enabled [" << misc::string::join(", ", protocolsEnabled) << "].");

          auto certificateKey=m_configWss.certificateKey;

          context->use_certificate_chain_file(m_configWss.certificateChain);
          context->use_private_key_file(certificateKey, WssContext::file_format::pem);
          context->use_tmp_dh_file(m_configWss.dhPrime);

          context->set_password_callback([&certificateKey](Size size, PasswordPurpose purpose) {
            String prompt="Enter passphrase for key ["+certificateKey+"]:";
            String password="";

            password.append(getpass(prompt.c_str()));

            return password;
          });

          return context;
        }
    };


    class WebsocketModule: public BrokerModule
    {
      public:
        virtual unique_ptr<Handler> handler(Json::Value const& configDefaults, Json::Value const& config, Stream* const sink) override
        {
          WsConfig configWs{
            WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_PORT),
            WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_HOST)
          };

          if(WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_ENABLED))
          {
            return make_unique<WebsocketHandler>(configWs, WssConfig{
              true,
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_PORT),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_HOST),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_DH_PRIME),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_PROTOCOLS),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_CIPHERS),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_CERTIFICATE_CHAIN),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_CERTIFICATE_KEY),
              WSBROKER_CONFIGS(configDefaults, config, HANDLER_WEBSOCKET_SSL_CERTIFICATE_KEY_PASSPHRASE)
            }, sink);
          }

          return make_unique<WebsocketHandler>(configWs, sink);
        }
    };
  }


  X_MODULE(websocket::WebsocketModule, "websocket", "1.0.0");
}
