/*
 * zeromq/module.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <config.h>

#include <x/common.h>

#include <wsbroker/context.h>


namespace wsbroker
{
  namespace zeromq
  {
    using namespace std;


    class ZeromqHandler: public Handler
    {
      public:
        virtual void start() override
        {
          dbgi("Start handler [zeromq].");
        }

        virtual void stop() override
        {
          dbgi("Stop handler [zeromq].");
        }


        virtual void operator<<(Message& message) override
        {

        }
    };


    class ZeromqModule: public BrokerModule
    {
      public:
        virtual unique_ptr<Handler> handler(Json::Value const& configDefaults, Json::Value const& config, Stream* const sink) override
        {
          return make_unique<ZeromqHandler>();
        }
    };
  }


  X_MODULE(zeromq::ZeromqModule, "zeromq", "1.0.0");
}
