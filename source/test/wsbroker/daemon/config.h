/*
 * daemon/config.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef TEST_WSBROKER_DAEMON_CONFIG_
#define TEST_WSBROKER_DAEMON_CONFIG_


#include <x/misc/test.h>

#include <x/io/resource/json.h>
#include <x/io/resources.h>


using namespace x;
using namespace x::io;


TEST(x_io_json_config, parse)
{
  String currentFile{__FILE__};
  auto idx=currentFile.find_last_of("/");

  String currentDirectory{currentFile.substr(0, idx)};

  Resources resources;
  auto resource=resources.get<resource::Json>("file://"+currentDirectory+"/asset/config.json");

  TIMER_BEGIN();
  auto& config=resource.resolve();

  TIMER_DUMP("Parsed");

  ASSERT_FALSE(resource.impl().hasError()) << resource.impl().error();

  ASSERT_TRUE(config.isObject());
  ASSERT_EQ(3, config.size());

  ASSERT_TRUE(config.at("handler").isArray());
  ASSERT_TRUE(config.at("route").isArray());
  ASSERT_TRUE(config.at("default").isObject());

  ASSERT_EQ(1, config.at("handler").at(0).at<Int>("id", 0));
  ASSERT_EQ(30, config.at("handler").at(2).at<Int>("id", 0));
  ASSERT_STREQ("11-20", config.at("handler").at(1).at<String>("id", "").c_str());
}


#endif
