/*
 * main.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <x/misc/test.h>

#include "daemon/config.h"


using namespace x;


Int main(Int argc, Char* argv[])
{
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
